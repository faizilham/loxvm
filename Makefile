TARGET   = lox

CC       = gcc
DEBUG	 =
# DEBUG	 += -DDEBUG_TRACE_EXECUTION
# DEBUG  += -DDEBUG_PRINT_CODE
# DEBUG  += -DDEBUG_STRESS_GC
# DEBUG  += -DDEBUG_LOG_GC
CFLAGS   = -std=c99 -Wall $(DEBUG) -I.
CFLAGS   += -O3

LINKER   = gcc
# linking flags here
LFLAGS   = -Wall -I. -lm

# change these to proper directories where each file should be
SRCDIR   = src
OBJDIR   = obj
BINDIR   = bin

SOURCES  := $(wildcard $(SRCDIR)/*.c)
INCLUDES := $(wildcard $(SRCDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
rm       = rm -f


$(BINDIR)/$(TARGET): $(OBJECTS)
	@$(LINKER) $(OBJECTS) $(LFLAGS) -o $@
	@echo "Linking complete!"

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiled "$<" successfully!"

.PHONY: all clean remove test
all: $(BINDIR)/$(TARGET)

clean:
	@$(rm) $(OBJECTS)
	@echo "Cleanup complete!"

remove: clean
	@$(rm) $(BINDIR)/$(TARGET)
	@echo "Executable removed!"

test:
	$(BINDIR)/$(TARGET) example/test.lox
