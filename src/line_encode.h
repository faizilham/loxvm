#ifndef clox_line_encode_h
#define clox_line_encode_h

typedef struct {
    int capacity;
    int count;
    int* indexes;
    int* lines;
} LineEncoding;

void initLineEncoding(LineEncoding* array);
void freeLineEncoding(LineEncoding* array);
void writeLineEncoding(LineEncoding* array, int line);
int findLineEncoding(LineEncoding* array, int index);

#endif