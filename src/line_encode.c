#include <stdlib.h>
#include <stdio.h>
#include "line_encode.h"
#include "memory.h"

void initLineEncoding(LineEncoding* array){
    array->count = 0;
    array->capacity = 0;
    array->indexes = NULL;
    array->lines = NULL;
}

void freeLineEncoding(LineEncoding* array){
    FREE_ARRAY(int, array->indexes, array->capacity);
    FREE_ARRAY(int, array->lines, array->capacity);
    initLineEncoding(array);
}

void writeLineEncoding(LineEncoding* array, int line){
    int lastIndex = array->count - 1;
    if (lastIndex >= 0 && array->lines[lastIndex] == line){
        array->indexes[lastIndex]++;
        return;
    }

    if (array->capacity < array->count + 1){
        int oldCapacity = array->capacity;
        array->capacity = GROW_CAPACITY(oldCapacity);
        array->lines = GROW_ARRAY(int, array->lines, oldCapacity, array->capacity);
        array->indexes = GROW_ARRAY(int, array->indexes, oldCapacity, array->capacity);
    }

    if (lastIndex >= 0){
        array->indexes[array->count] = array->indexes[lastIndex] + 1;
    } else {
        array->indexes[array->count] = 0;
    }

    array->lines[array->count] = line;
    array->count++;
}

int findLineEncoding(LineEncoding* array, int index){
    int i = 0;

    while (i < array->count && array->indexes[i] < index){
        i++;
    }

    if (i < array->count){
        return array->lines[i];
    }

    return 0;
}
