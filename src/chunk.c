#include <stdlib.h>
#include "chunk.h"
#include "memory.h"
#include "line_encode.h"
#include "value.h"
#include "vm.h"

void initChunk(Chunk* chunk){
    chunk->count = 0;
    chunk->capacity = 0;
    chunk->code = NULL;
    initLineEncoding(&chunk->lines);
    initValueArray(&chunk->constants);
}

void freeChunk(Chunk* chunk){
    FREE_ARRAY(uint8_t, chunk->code, chunk->capacity);
    freeLineEncoding(&chunk->lines);
    freeValueArray(&chunk->constants);
    initChunk(chunk);
}

void writeChunk(Chunk* chunk, uint8_t byte, int line){
    if (chunk->capacity < chunk->count + 1){
        int oldCapacity = chunk->capacity;
        chunk->capacity = GROW_CAPACITY(oldCapacity);
        chunk->code = GROW_ARRAY(uint8_t, chunk->code, oldCapacity, chunk->capacity);
    }

    chunk->code[chunk->count] = byte;
    writeLineEncoding(&chunk->lines, line);
    chunk->count++;
}

int getLine(Chunk* chunk, int index){
    return findLineEncoding(&chunk->lines, index);
}

int addConstant(Chunk* chunk, Value value){
    // check uniqueness for strings and non-objects
    if (!IS_OBJ(value) || IS_STRING(value)) {
        for (int i = 0; i < chunk->constants.count; i++) {
            if (valuesEqual(chunk->constants.values[i], value)){
                return i;
            }
        }
    }

    push(value); // gc guard
    writeValueArray(&chunk->constants, value);
    pop();
    return chunk->constants.count - 1;
}
