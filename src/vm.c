#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "common.h"
#include "compiler.h"
#include "debug.h"
#include "vm.h"
#include "memory.h"
#include "object.h"

VM vm;

static Value clockNative(int argCount, Value* args) {
    return NUMBER_VAL((double) clock()/ CLOCKS_PER_SEC);
}

static void resetStack(){
    vm.stackTop = vm.stack;
    vm.frameCount = 0;
    vm.openUpvalues = NULL;
}

static void runtimeError(uint8_t* currentIp, const char* format, ...) {
    va_list args;

    fputs("Runtime Error: ", stderr);

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    fputs("\n", stderr);

    uint8_t* ip = currentIp;

    for (int i = vm.frameCount - 1; i >= 0; i--) {
        CallFrame* frame = &vm.frames[i];
        ObjFunction* function = frame->isClosure ? frame->callee.closure->function : frame->callee.function;
        size_t instruction = ip - function->chunk.code - 1;
        fprintf(stderr, "    [line %d] in ", getLine(&function->chunk, instruction));
        if (function->name == NULL) {
            fprintf(stderr, "script\n");
        } else {
            fprintf(stderr, "%s()\n", function->name->chars);
        }

        ip = frame->returnIp;
    }

    resetStack();
}

static void defineNative(const char* name, NativeFn function, int arity) {
    push(OBJ_VAL(copyString(name, (int)strlen(name))));
    push(OBJ_VAL(newNative(function, arity)));
    tableSet(&vm.globals, AS_STRING(vm.stack[0]), vm.stack[1]);
    pop();
    pop();
}

void initVM(){
    resetStack();
    vm.objects = NULL;
    vm.bytesAllocated = 0;
    vm.nextGC = 1024 * 1024;
    initTable(&vm.globals);
    initTable(&vm.strings);
    vm.initString = NULL; // gc guard
    vm.initString = copyString("init", 4);

    vm.grayCount = 0;
    vm.grayCapacity = 0;
    vm.grayStack = NULL;

    defineNative("clock", clockNative, 0);
}

void freeVM(){
    freeTable(&vm.globals);
    freeTable(&vm.strings);
    vm.initString = NULL;
    freeObjects();
}

void push(Value value){
    *vm.stackTop = value;
    vm.stackTop++;
}

Value pop(){
    vm.stackTop--;
    return *vm.stackTop;
}

static Value peek(int distance) {
    return vm.stackTop[-1 - distance];
}

static uint8_t* call(uint8_t* returnIp, ObjFunction* function, int argCount) {
    if (argCount != function->arity) {
        runtimeError(returnIp, "Expected %d arguments but got %d.",
            function->arity, argCount);
        return NULL;
    }

    if (vm.frameCount == FRAMES_MAX) {
        runtimeError(returnIp, "Stack overflow");
        return NULL;
    }

    CallFrame* frame = &vm.frames[vm.frameCount++];
    frame->callee.function = function;
    frame->isClosure = false;
    frame->returnIp = returnIp;
    frame->slots = vm.stackTop - argCount - 1;

    return function->chunk.code;
}

static uint8_t* callClosure(uint8_t* returnIp, ObjClosure* closure, int argCount) {
    if (argCount != closure->function->arity) {
        runtimeError(returnIp, "Expected %d arguments but got %d.",
            closure->function->arity, argCount);
        return NULL;
    }

    if (vm.frameCount == FRAMES_MAX) {
        runtimeError(returnIp, "Stack overflow");
        return NULL;
    }

    CallFrame* frame = &vm.frames[vm.frameCount++];
    frame->callee.closure = closure;
    frame->isClosure = true;
    frame->returnIp = returnIp;
    frame->slots = vm.stackTop - argCount - 1;

    return closure->function->chunk.code;
}

static uint8_t* callNative(uint8_t* returnIp, ObjNative* objNative, int argCount) {
    if (argCount != objNative->arity) {
        runtimeError(returnIp, "Expected %d arguments but got %d.", objNative->arity, argCount);
        return NULL;
    }

    NativeFn native = objNative->function;

    Value result = native(argCount, vm.stackTop - argCount);
    vm.stackTop -= argCount + 1;
    push(result);
    return returnIp;
}

static bool callValue(Value callee, int argCount, uint8_t* returnIp, uint8_t** nextIp) {
    if (IS_OBJ(callee)) {
        switch(OBJ_TYPE(callee)) {
            case OBJ_BOUND_METHOD: {
                ObjBoundMethod* bound = AS_BOUND_METHOD(callee);
                vm.stackTop[-argCount - 1] = bound->receiver;
                *nextIp = callClosure(returnIp, bound->method, argCount);
                return *nextIp != NULL;
            }
            case OBJ_CLASS: {
                ObjClass* klass = AS_CLASS(callee);
                vm.stackTop[-argCount - 1] = OBJ_VAL(newInstance(klass));
                if (klass->init != NULL) {
                    *nextIp = callClosure(returnIp, klass->init, argCount);
                    return *nextIp != NULL;
                } else if (argCount != 0) {
                    runtimeError(returnIp, "Expected 0 arguments but got %d.", argCount);
                    return false;
                }
                return true;
            }
            case OBJ_FUNCTION:
                *nextIp = call(returnIp, AS_FUNCTION(callee), argCount);
                return *nextIp != NULL;
            case OBJ_CLOSURE:
                *nextIp = callClosure(returnIp, AS_CLOSURE(callee), argCount);
                return *nextIp != NULL;
            case OBJ_NATIVE: {
                *nextIp = callNative(returnIp, AS_OBJNATIVE(callee), argCount);
                return *nextIp != NULL;
            }
            default:
                break;
        }
    }

    runtimeError(returnIp, "Can only call functions and classes.");
    return false;
}

static bool invokeFromClass(ObjClass* klass, ObjString* name, int argCount, uint8_t* currentIp, uint8_t** nextIp) {
    Value method;
    if (!tableGet(&klass->methods, name, &method)) {
        runtimeError(currentIp, "Unknown method '%s'.", name->chars);
        return false;
    }

    *nextIp = callClosure(currentIp, AS_CLOSURE(method), argCount);

    return *nextIp != NULL;
}

static bool invoke(ObjString* name, int argCount, uint8_t* currentIp, uint8_t** nextIp) {
    Value receiver = peek(argCount);
    if (!IS_INSTANCE(receiver)) {
        runtimeError(currentIp, "Only instances have methods.");
        return false;
    }

    ObjInstance* instance = AS_INSTANCE(receiver);

    Value value;
    if (tableGet(&instance->fields, name, &value)) {
        vm.stackTop[-argCount - 1] = value;

        return callValue(value, argCount, currentIp, nextIp);
    }

    return invokeFromClass(instance->klass, name, argCount, currentIp, nextIp);
}

static Value bindMethod(ObjClass* klass, ObjString* name, Value receiver) {
    Value method;
    if (!tableGet(&klass->methods, name, &method)) {
        return NIL_VAL;
    }

    ObjBoundMethod* bound = newBoundMethod(receiver, AS_CLOSURE(method));
    return OBJ_VAL(bound);
}

static ObjUpvalue* captureUpvalue(Value* local) {
    ObjUpvalue* prevUpvalue = NULL;
    ObjUpvalue* upvalue = vm.openUpvalues;
    while(upvalue != NULL && upvalue->location > local) {
        prevUpvalue = upvalue;
        upvalue = upvalue->next;
    }

    if (upvalue != NULL && upvalue->location == local) {
        return upvalue;
    }

    ObjUpvalue* createdUpvalue = newUpvalue(local);
    createdUpvalue->next = upvalue;

    if (prevUpvalue == NULL) {
        vm.openUpvalues = createdUpvalue;
    } else {
        prevUpvalue->next = createdUpvalue;
    }
    return createdUpvalue;
}

static void closeUpvalues(Value* last) {
    while (vm.openUpvalues != NULL && vm.openUpvalues->location >= last) {
        ObjUpvalue* upvalue = vm.openUpvalues;
        upvalue->closed = *upvalue->location;
        upvalue->location = &upvalue->closed;
        vm.openUpvalues = upvalue->next;
    }
}

static void defineMethod(ObjString* name, bool isInitializer) {
    Value method = peek(0);
    ObjClass* klass = AS_CLASS(peek(1));
    tableSet(&klass->methods, name, method);

    if (isInitializer) {
        klass->init = AS_CLOSURE(method);
    }

    pop();
}

static void replaceTop(Value value) {
    vm.stackTop[-1] = value;
}

static void concatenate() {
    ObjString* b = AS_STRING(peek(0));
    ObjString* a = AS_STRING(peek(1));
    ObjString* result = concatString(a, b);

    pop(); // gc guard
    pop();
    push(OBJ_VAL(result));
}

static ObjFunction* getCurrentFunction(CallFrame* frame) {
    return frame->isClosure ? frame->callee.closure->function : frame->callee.function;
}

static InterpretResult run(){
    CallFrame* frame = &vm.frames[vm.frameCount-1];
    ObjFunction* currentFunction = getCurrentFunction(frame);
    register uint8_t* ip = currentFunction->chunk.code;

#define READ_BYTE() (*ip++)
#define READ_CONSTANT() (currentFunction->chunk.constants.values[READ_BYTE()])
#define READ_SHORT() \
    (ip += 2, (uint16_t)((ip[-2] << 8) | ip[-1]))
#define READ_STRING() AS_STRING(READ_CONSTANT())
#define BINARY_OP(valueType, op) \
    do { \
        if (!IS_NUMBER(peek(0)) || !IS_NUMBER(peek(1))) { \
            runtimeError(ip, "Operands must be numbers."); \
            return INTERPRET_RUNTIME_ERROR; \
        } \
        \
        double b = AS_NUMBER(pop()); \
        double a = AS_NUMBER(pop()); \
        push(valueType(a op b)); \
    } while(false)

    for(;;){

        #ifdef DEBUG_TRACE_EXECUTION
            printf("          ");
            for (Value* slot = vm.stack; slot < vm.stackTop; slot++){
                printf("[ ");
                printValue(*slot);
                printf(" ]");
            }
            printf("\n");
            disassembleInstruction(&currentFunction->chunk,
                (int)(ip - currentFunction->chunk.code));
        #endif

        uint8_t instruction;
        switch(instruction = READ_BYTE()){
            case OP_CONSTANT:{
                Value constant = READ_CONSTANT();
                push(constant);
                break;
            }
            case OP_NIL: push(NIL_VAL); break;
            case OP_TRUE: push(BOOL_VAL(true)); break;
            case OP_FALSE: push(BOOL_VAL(false)); break;
            case OP_POP: pop(); break;
            case OP_GET_LOCAL: {
                uint8_t slot = READ_BYTE();
                push(frame->slots[slot]);
                break;
            }
            case OP_SET_LOCAL: {
                uint8_t slot = READ_BYTE();
                frame->slots[slot] = peek(0);
                break;
            }
            case OP_GET_GLOBAL: {
                ObjString* name = READ_STRING();
                Value value;
                if (!tableGet(&vm.globals, name, &value)) {
                    runtimeError(ip, "Undefined variable '%s'.", name->chars);
                    return INTERPRET_RUNTIME_ERROR;
                }

                push(value);
                break;
            }
            case OP_DEFINE_GLOBAL: {
                ObjString* name = READ_STRING();
                tableSet(&vm.globals, name, peek(0));
                // peek then pop to guarantee object is tracable if gc is triggered in the middle of tableSet
                pop();
                break;
            }
            case OP_SET_GLOBAL: {
                ObjString* name = READ_STRING();
                if (tableSet(&vm.globals, name, peek(0))) {
                    tableDelete(&vm.globals, name);
                    runtimeError(ip, "Undefined variable '%s'.", name->chars);
                    return INTERPRET_RUNTIME_ERROR;
                }
                break;
            }
            case OP_GET_UPVALUE: {
                uint8_t slot = READ_BYTE();
                push(*frame->callee.closure->upvalues[slot]->location);
                break;
            }
            case OP_SET_UPVALUE: {
                uint8_t slot = READ_BYTE();
                *frame->callee.closure->upvalues[slot]->location = peek(0);
                break;
            }
            case OP_GET_PROPERTY: {
                Value instanceVal = peek(0);
                if (!IS_INSTANCE(instanceVal)) {
                    runtimeError(ip, "Only instances have properties.");
                    return INTERPRET_RUNTIME_ERROR;
                }

                ObjInstance* instance = AS_INSTANCE(instanceVal);
                ObjString* name = READ_STRING();
                Value value = NIL_VAL;
                if (!tableGet(&instance->fields, name, &value)) {
                    value = bindMethod(instance->klass, name, instanceVal);
                }

                pop(); // instance
                push(value);

                break;
            }
            case OP_SET_PROPERTY: {
                if (!IS_INSTANCE(peek(1))) {
                    runtimeError(ip, "Only instances have properties.");
                    return INTERPRET_RUNTIME_ERROR;
                }

                ObjInstance* instance = AS_INSTANCE(peek(1));
                tableSet(&instance->fields, READ_STRING(), peek(0));
                Value value = pop();
                pop();
                push(value);
                break;
            }
            case OP_GET_INDEX: {
                Value instanceVal = peek(1);
                Value propVal = peek(0);

                if (!IS_INSTANCE(instanceVal)) {
                    runtimeError(ip, "Only instances have properties.");
                    return INTERPRET_RUNTIME_ERROR;
                }

                if (!IS_STRING(propVal)) {
                    runtimeError(ip, "Instance property must be a string");
                    return INTERPRET_RUNTIME_ERROR;
                }

                ObjInstance* instance = AS_INSTANCE(instanceVal);
                ObjString* name = AS_STRING(propVal);
                Value value;
                if (!tableGet(&instance->fields, name, &value)) {
                    value = bindMethod(instance->klass, name, instanceVal);
                }

                pop(); // property
                pop(); // instance
                push(value);

                break;
            }
            case OP_SET_INDEX: {
                Value instanceVal = peek(2);
                Value propVal = peek(1);

                if (!IS_INSTANCE(instanceVal)) {
                    runtimeError(ip, "Only instances have properties.");
                    return INTERPRET_RUNTIME_ERROR;
                }

                if (!IS_STRING(propVal)) {
                    runtimeError(ip, "Instance property must be a string");
                    return INTERPRET_RUNTIME_ERROR;
                }

                ObjInstance* instance = AS_INSTANCE(instanceVal);
                ObjString* name = AS_STRING(propVal);

                tableSet(&instance->fields, name, peek(0));
                Value value = pop();
                pop(); // property
                pop(); // instance
                push(value);
                break;
            }
            case OP_GET_SUPER: {
                ObjString* name = READ_STRING();
                ObjClass* superclass = AS_CLASS(pop());
                Value instanceVal = peek(0);

                Value bound = bindMethod(superclass, name, instanceVal);

                if (IS_NIL(bound)) {
                    runtimeError(ip, "Unknown super method '%s'.", name->chars);
                    return INTERPRET_RUNTIME_ERROR;
                }

                pop(); // 'this' instance
                push(bound);
                break;
            }
            case OP_EQUAL: {
                Value b = pop();
                Value a = pop();
                push(BOOL_VAL(valuesEqual(a, b)));
                break;
            }
            case OP_GREATER:    BINARY_OP(BOOL_VAL, >); break;
            case OP_LESS:       BINARY_OP(BOOL_VAL, <); break;
            case OP_ADD:
                if (IS_STRING(peek(0)) && IS_STRING(peek(1))) {
                    concatenate();
                } else if (IS_NUMBER(peek(0)) && IS_NUMBER(peek(1))) {
                    double b = AS_NUMBER(pop());
                    double a = AS_NUMBER(pop());
                    push(NUMBER_VAL(a + b));
                } else {
                    runtimeError(ip, "Operand must be two numbers or two strings.");
                    return INTERPRET_RUNTIME_ERROR;
                }
            break;
            case OP_SUBTRACT:   BINARY_OP(NUMBER_VAL, -); break;
            case OP_MULTIPLY:   BINARY_OP(NUMBER_VAL, *); break;
            case OP_DIVIDE:     BINARY_OP(NUMBER_VAL, /); break;
            case OP_NOT:
                replaceTop(BOOL_VAL(isFalsey(peek(0))));
            break;
            case OP_NEGATE:

                if (!IS_NUMBER(peek(0))){
                    runtimeError(ip, "Operand must be a number.");
                    return INTERPRET_RUNTIME_ERROR;
                }

                replaceTop(NUMBER_VAL(-AS_NUMBER(peek(0))));
            break;
            case OP_PRINT: {
                printValue(pop());
                printf("\n");
                break;
            }
            case OP_JUMP: {
                uint16_t offset = READ_SHORT();
                ip += offset;
                break;
            }
            case OP_JUMP_IF_FALSE: {
                uint16_t offset = READ_SHORT();
                if (isFalsey(peek(0))) ip += offset;
                break;
            }
            case OP_LOOP: {
                uint16_t offset = READ_SHORT();
                ip -= offset;
                break;
            }
            case OP_CALL: {
                int argCount = READ_BYTE();
                uint8_t* next = NULL;

                if (!callValue(peek(argCount), argCount, ip, &next)) {
                    return INTERPRET_RUNTIME_ERROR;
                }

                if (next != NULL) { // class instantiation instead of function call
                    frame = &vm.frames[vm.frameCount - 1];
                    currentFunction = getCurrentFunction(frame);
                    ip = next;
                }

                break;
            }
            case OP_INVOKE: {
                ObjString* method = READ_STRING();
                int argCount = READ_BYTE();
                uint8_t* next = NULL;

                if (!invoke(method, argCount, ip, &next)) {
                    return INTERPRET_RUNTIME_ERROR;
                }

                if (next != NULL) {
                    frame = &vm.frames[vm.frameCount - 1];
                    currentFunction = getCurrentFunction(frame);
                    ip = next;
                }

                break;
            }
            case OP_SUPER_INVOKE: {
                ObjString* method = READ_STRING();
                int argCount = READ_BYTE();
                ObjClass* superclass = AS_CLASS(pop());
                uint8_t* next = NULL;

                if (!invokeFromClass(superclass, method, argCount, ip, &next)) {
                    return INTERPRET_RUNTIME_ERROR;
                }

                frame = &vm.frames[vm.frameCount - 1];
                currentFunction = getCurrentFunction(frame);

                ip = next;
                break;
            }
            case OP_CLOSURE: {
                ObjFunction* function = AS_FUNCTION(READ_CONSTANT());
                ObjClosure* closure = newClosure(function);
                push(OBJ_VAL(closure));
                for (int i = 0; i < closure->upvalueCount; i++) {
                    uint8_t isLocal = READ_BYTE();
                    uint8_t index = READ_BYTE();

                    if (isLocal) {
                        closure->upvalues[i] = captureUpvalue(frame->slots + index);
                    } else {
                        closure->upvalues[i] = frame->callee.closure->upvalues[index];
                    }
                }
                break;
            }
            case OP_CLOSE_UPVALUE: {
                closeUpvalues(vm.stackTop - 1);
                pop();
                break;
            }
            case OP_RETURN: {
                Value result = pop();
                CallFrame* lastFrame = &vm.frames[vm.frameCount - 1];
                uint8_t* returnIp = lastFrame->returnIp;

                closeUpvalues(frame->slots);
                vm.frameCount--;
                if (vm.frameCount == 0) {
                    pop(); // pop the main function object
                    return INTERPRET_OK;
                }

                vm.stackTop = frame->slots;
                push(result);
                frame = &vm.frames[vm.frameCount - 1];
                currentFunction = getCurrentFunction(frame);
                ip = returnIp;
                break;
            }
            case OP_CLASS: {
                push(OBJ_VAL(newClass(READ_STRING())));
                break;
            }
            case OP_INHERIT :{
                Value superclass = peek(1);
                if (!IS_CLASS(superclass)) {
                    runtimeError(ip, "Superclass must be a class.");
                    return INTERPRET_RUNTIME_ERROR;
                }
                ObjClass* subclass = AS_CLASS(peek(0));
                tableAddAll(&AS_CLASS(superclass)->methods, &subclass->methods);
                pop(); // subclass
                break;
            }
            case OP_METHOD: {
                defineMethod(READ_STRING(), false);
                break;
            }
            case OP_INITIALIZER: {
                defineMethod(READ_STRING(), true);
                break;
            }
        }
    }

#undef READ_BYTE
#undef READ_SHORT
#undef READ_CONSTANT
#undef READ_STRING
#undef BINARY_OP
}

InterpretResult interpret(const char* source){
    ObjFunction* function = compile(source);
    if (function == NULL) return INTERPRET_COMPILE_ERROR;

    push(OBJ_VAL(function)); // gc guard
    call(NULL, function, 0); // should be ok to use NULL as return here

    return run();
}
